package com.realwear.bluetooth.di

import com.realwear.bluetooth.model.BluetoothConnector
import com.realwear.bluetooth.model.BluetoothConnectorImpl
import com.realwear.bluetooth.model.BluetoothScanner
import com.realwear.bluetooth.model.BluetoothScannerImpl
import org.koin.android.ext.koin.androidContext
import org.koin.dsl.module.module

val bluetoothConnectionModule = module {
    single { BluetoothConnectorImpl(androidContext()) as BluetoothConnector }
    factory { BluetoothScannerImpl(androidContext()) as BluetoothScanner }

}