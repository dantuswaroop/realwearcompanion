package com.realwear.bluetooth.di

import com.realwear.bluetooth.service.connection.ConnectionController
import org.koin.dsl.module.module

val bluetoothApplicationModule = module {
    factory { params ->
        ConnectionController(params[0], params[1], get())
    }

}
