package com.realwear.bluetooth.model

import android.bluetooth.BluetoothDevice
import android.content.ComponentName
import android.content.Context
import android.content.ServiceConnection
import android.os.IBinder
import com.realwear.bluetooth.internal.CommunicationProxy
import com.realwear.bluetooth.internal.EmptyProxy
import com.realwear.bluetooth.service.BluetoothConnectionService
import com.realwear.bluetooth.utils.safeRemove

class BluetoothConnectorImpl(private val context: Context) : BluetoothConnector {

    private val monitor = Any()

    private var prepareListeners = LinkedHashSet<OnPrepareListener>()
    private var connectListeners = LinkedHashSet<OnConnectionListener>()
    private var messageListeners = LinkedHashSet<OnMessageListener>()

    private var proxy: CommunicationProxy? = null

    private var service: BluetoothConnectionService? = null
    private var bound = false
    private var isPreparing = false

    private val connection = object : ServiceConnection {

        override fun onServiceConnected(className: ComponentName, binder: IBinder) {

            service = (binder as BluetoothConnectionService.ConnectionBinder).getService().apply {
                setConnectionListener(connectionListenerInner)
                setMessageListener(messageListenerInner)
            }

            proxy = EmptyProxy()

            bound = true
            isPreparing = false
            synchronized(monitor) {
                prepareListeners.forEach { it.onPrepared() }
            }
        }

        override fun onServiceDisconnected(className: ComponentName) {
            service?.setConnectionListener(null)
            service?.setMessageListener(null)
            service = null
            proxy = null

            isPreparing = false
            bound = false
            synchronized(monitor) {
                prepareListeners.forEach { it.onError() }
            }
        }
    }

    private val connectionListenerInner = object : OnConnectionListener {

        override fun onConnected(device: BluetoothDevice) {
            proxy?.onConnected(device)
            synchronized(monitor) {
                connectListeners.forEach { it.onConnected(device) }
            }
        }

        override fun onConnectionWithdrawn() {
            proxy?.onConnectionWithdrawn()
            synchronized(monitor) {
                connectListeners.forEach { it.onConnectionWithdrawn() }
            }
        }

        override fun onConnectionAccepted() {
            proxy?.onConnectionAccepted()
            synchronized(monitor) {
                connectListeners.forEach { it.onConnectionAccepted() }
            }
        }

        override fun onConnectionRejected() {
            proxy?.onConnectionRejected()
            synchronized(monitor) {
                connectListeners.forEach { it.onConnectionRejected() }
            }
        }

        override fun onConnecting() {
            proxy?.onConnecting()
            synchronized(monitor) {
                connectListeners.forEach { it.onConnecting() }
            }
        }

        override fun onConnectedIn(conversation: String) {
            proxy?.onConnectedIn(conversation)
            synchronized(monitor) {
                connectListeners.forEach { it.onConnectedIn(conversation) }
            }
        }

        override fun onConnectedOut(conversation: String) {
            proxy?.onConnectedOut(conversation)
            synchronized(monitor) {
                connectListeners.forEach { it.onConnectedOut(conversation) }
            }
        }

        override fun onConnectionLost() {
            proxy?.onConnectionLost()
            synchronized(monitor) {
                connectListeners.forEach { it.onConnectionLost() }
            }
        }

        override fun onConnectionFailed() {
            proxy?.onConnectionFailed()
            synchronized(monitor) {
                connectListeners.forEach { it.onConnectionFailed() }
            }
        }

        override fun onDisconnected() {
            proxy?.onDisconnected()
            synchronized(monitor) {
                connectListeners.forEach { it.onDisconnected() }
            }
        }

        override fun onConnectionDestroyed() {
            proxy?.onConnectionDestroyed()
            synchronized(monitor) {
                connectListeners.forEach { it.onConnectionDestroyed() }
            }
            release()
        }
    }

    private val messageListenerInner = object : OnMessageListener {

        override fun onMessageReceived(message: String) {
            proxy?.onMessageReceived(message)
            synchronized(monitor) {
                messageListeners.forEach { it.onMessageReceived(message) }
            }
        }

        override fun onMessageSent(message: String) {
            proxy?.onMessageSent(message)
            synchronized(monitor) {
                messageListeners.forEach { it.onMessageSent(message) }
            }
        }

        override fun onMessageSendingFailed() {
            proxy?.onMessageSendingFailed()
            synchronized(monitor) {
                messageListeners.forEach { it.onMessageSendingFailed() }
            }
        }

        override fun onMessageDelivered(id: Long) {
            proxy?.onMessageDelivered(id)
            synchronized(monitor) {
                messageListeners.forEach { it.onMessageDelivered(id) }
            }
        }

        override fun onMessageNotDelivered(id: Long) {
            proxy?.onMessageNotDelivered(id)
            synchronized(monitor) {
                messageListeners.forEach { it.onMessageNotDelivered(id) }
            }
        }

        override fun onMessageSeen(id: Long) {
            proxy?.onMessageSeen(id)
            synchronized(monitor) {
                messageListeners.forEach { it.onMessageSeen(id) }
            }
        }
    }

    override fun prepare() {

        if (isPreparing) return
        isPreparing = true

        bound = false
        if (!BluetoothConnectionService.isRunning) {
            BluetoothConnectionService.start(context)
        }
        BluetoothConnectionService.bind(context, connection)
    }

    override fun release() {

        if (bound) {
            context.unbindService(connection)
        }

        bound = false
        service = null
        proxy = null

        synchronized(monitor) {
            connectListeners = LinkedHashSet()
            prepareListeners = LinkedHashSet()
            messageListeners = LinkedHashSet()
        }
    }

    override fun isConnectionPrepared() = bound

    override fun addOnPrepareListener(listener: OnPrepareListener) {
        synchronized(monitor) {
            prepareListeners.add(listener)
        }
    }

    override fun addOnConnectListener(listener: OnConnectionListener) {
        synchronized(monitor) {
            connectListeners.add(listener)
        }
    }

    override fun addOnMessageListener(listener: OnMessageListener) {
        synchronized(monitor) {
            messageListeners.add(listener)
        }
    }

    override fun removeOnPrepareListener(listener: OnPrepareListener) {
        synchronized(monitor) {
            prepareListeners.safeRemove(listener)
        }
    }

    override fun removeOnConnectListener(listener: OnConnectionListener) {
        synchronized(monitor) {
            connectListeners.safeRemove(listener)
        }
    }

    override fun removeOnMessageListener(listener: OnMessageListener) {
        synchronized(monitor) {
            messageListeners.safeRemove(listener)
        }
    }

    override fun connect(device: BluetoothDevice) {
        service?.connect(device)
    }

    override fun stop() {
        service?.stop()
    }

    override fun sendMessage(messageText: String) {

        service?.getCurrentContract()?.createChatMessage(messageText)?.let { message ->
            service?.sendMessage(message)
        }
    }

    override fun disconnect() {
        service?.disconnect()
    }

    override fun isConnected() = service?.isConnected() ?: false

    override fun isConnectedOrPending() = service?.isConnectedOrPending() ?: false

    override fun isPending() = service?.isPending() ?: false

    override fun acceptConnection() {
        service?.approveConnection()
    }

    override fun rejectConnection() {
        service?.rejectConnection()
    }

    override fun sendDisconnectRequest() {
        service?.getCurrentContract()?.createDisconnectMessage()?.let { message ->
            service?.sendMessage(message)
        }
    }

}
