package com.realwear.bluetooth.model

interface OnPrepareListener {
    fun onPrepared()
    fun onError()
}