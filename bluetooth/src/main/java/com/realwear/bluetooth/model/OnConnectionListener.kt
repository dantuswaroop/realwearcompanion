package com.realwear.bluetooth.model

import android.bluetooth.BluetoothDevice
interface OnConnectionListener {
    fun onConnecting()
    fun onConnected(device: BluetoothDevice)
    fun onConnectionLost()
    fun onConnectionFailed()
    fun onConnectionDestroyed()
    fun onDisconnected()
    fun onConnectionAccepted()
    fun onConnectionRejected()
    fun onConnectionWithdrawn()
    fun onConnectedIn(conversation: String)
    fun onConnectedOut(conversation: String)
}
