package com.realwear.bluetooth.ui.view

import android.app.Notification

interface NotificationView {

    companion object {

        const val ACTION_CONNECTION = "action.connection"
        const val EXTRA_APPROVED = "extra.approved"
        const val EXTRA_ADDRESS = "extra.address"
        const val ACTION_REPLY = "action.reply"
        const val EXTRA_TEXT_REPLY = "extra.text_reply"

        const val NOTIFICATION_ID_MESSAGE = 7438925
        const val NOTIFICATION_ID_CONNECTION = 5438729
        const val NOTIFICATION_ID_FILE = 1415665

        const val NOTIFICATION_TAG_MESSAGE = "tag.message"
        const val NOTIFICATION_TAG_CONNECTION = "tag.connection"
        const val NOTIFICATION_TAG_FILE = "tag.file"

        const val CHANNEL_FOREGROUND = "channel.foreground"
        const val CHANNEL_REQUEST = "channel.request"
        const val CHANNEL_MESSAGE = "channel.message"
        const val CHANNEL_FILE = "channel.file"
    }

    fun getForegroundNotification(message: String): Notification
    fun showConnectionRequestNotification(deviceName: String, address: String, soundEnabled: Boolean)
}
