package com.realwear.bluetooth.internal

import android.bluetooth.BluetoothDevice

class EmptyProxy : CommunicationProxy {
    override fun onConnectedIn(conversation: String) {}
    override fun onConnectedOut(conversation: String) {}
    override fun onConnecting() {}
    override fun onConnected(device: BluetoothDevice) {}
    override fun onConnectionLost() {}
    override fun onConnectionFailed() {}
    override fun onConnectionDestroyed() {}
    override fun onDisconnected() {}
    override fun onConnectionAccepted() {}
    override fun onConnectionRejected() {}
    override fun onConnectionWithdrawn() {}
    override fun onMessageReceived(message: String) {}
    override fun onMessageSent(message: String) {}
    override fun onMessageSendingFailed() {}
    override fun onMessageDelivered(id: Long) {}
    override fun onMessageNotDelivered(id: Long) {}
    override fun onMessageSeen(id: Long) {}
}
