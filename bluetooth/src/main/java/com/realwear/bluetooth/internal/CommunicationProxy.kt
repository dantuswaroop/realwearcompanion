package com.realwear.bluetooth.internal

import android.bluetooth.BluetoothDevice
interface CommunicationProxy {

    fun onConnectedIn(conversation: String)
    fun onConnectedOut(conversation: String)
    fun onConnecting()
    fun onConnected(device: BluetoothDevice)
    fun onConnectionLost()
    fun onConnectionFailed()
    fun onConnectionDestroyed()
    fun onDisconnected()
    fun onConnectionAccepted()
    fun onConnectionRejected()
    fun onConnectionWithdrawn()

    fun onMessageReceived(message: String)
    fun onMessageSent(message: String)
    fun onMessageSendingFailed()
    fun onMessageDelivered(id: Long)
    fun onMessageNotDelivered(id: Long)
    fun onMessageSeen(id: Long)
}
