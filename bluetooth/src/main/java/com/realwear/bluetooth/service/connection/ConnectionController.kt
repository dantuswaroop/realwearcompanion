package com.realwear.bluetooth.service.connection

import android.app.Application
import android.bluetooth.BluetoothAdapter
import android.bluetooth.BluetoothDevice
import android.bluetooth.BluetoothServerSocket
import android.bluetooth.BluetoothSocket
import androidx.core.app.NotificationCompat
import androidx.core.app.Person
import com.realwear.bluetooth.R
import com.realwear.bluetooth.service.message.Contract
import com.realwear.bluetooth.service.message.Message
import com.realwear.bluetooth.ui.view.NotificationView
import com.realwear.bluetooth.utils.LimitedQueue
import kotlinx.coroutines.*
import java.io.IOException
import java.util.*
import kotlin.coroutines.CoroutineContext

class ConnectionController(
    private val application: Application,
    private val subject: ConnectionSubject,
    private val view: NotificationView,
    private val uiContext: CoroutineDispatcher = Dispatchers.Main,
    private val bgContext: CoroutineDispatcher = Dispatchers.IO
) : CoroutineScope {

    private val blAppName = application.getString(R.string.bl_app_name)
    private val blAppUUID = UUID.fromString(application.getString(R.string.bl_app_uuid))

    private var acceptThread: AcceptJob? = null
    private var connectThread: ConnectJob? = null
    private var dataTransferThread: DataTransferThread? = null

    @Volatile
    private var connectionState: ConnectionState = ConnectionState.NOT_CONNECTED

    @Volatile
    private var connectionType: ConnectionType? = null

    private var currentSocket: BluetoothSocket? = null
    private var contract = Contract()

    private var justRepliedFromNotification = false

    private val me: Person by lazy {
        Person.Builder().setName("Me").build()
    }
    private val shallowHistory = LimitedQueue<NotificationCompat.MessagingStyle.Message>(4)

    var onNewForegroundMessage: ((String) -> Unit)? = null

    private val job = Job()

    override val coroutineContext: CoroutineContext
        get() = job + uiContext

    fun createForegroundNotification(message: String) = view.getForegroundNotification(message)

    @Synchronized
    fun prepareForAccept() {

        cancelConnections()

        if (subject.isRunning()) {
            acceptThread = AcceptJob()
            acceptThread?.start()
            onNewForegroundMessage?.invoke(application.getString(R.string.notification__ready_to_connect))
        }
    }

    @Synchronized
    fun disconnect() {
        dataTransferThread?.cancel(true)
        dataTransferThread = null
        prepareForAccept()
    }

    @Synchronized
    fun connect(device: BluetoothDevice) {

        if (connectionState == ConnectionState.CONNECTING) {
            connectThread?.cancel()
            connectThread = null
        }

        dataTransferThread?.cancel(true)
        acceptThread?.cancel()
        dataTransferThread = null
        acceptThread = null
        currentSocket = null
        contract.reset()
        connectionType = null

        connectThread = ConnectJob(device)
        connectThread?.start()

        launch { subject.handleConnectingInProgress() }
    }

    private fun cancelConnections() {
        connectThread?.cancel()
        connectThread = null
        dataTransferThread?.cancel()
        dataTransferThread = null
        currentSocket = null
        contract.reset()
        connectionType = null
        justRepliedFromNotification = false
    }

    private fun cancelAccept() {
        acceptThread?.cancel()
        acceptThread = null
    }

    private fun connectionFailed() {
        currentSocket = null
        contract.reset()
        launch { subject.handleConnectionFailed() }
        connectionState = ConnectionState.NOT_CONNECTED
        prepareForAccept()
    }

    @Synchronized
    fun stop() {
        cancelConnections()
        cancelAccept()
        connectionState = ConnectionState.NOT_CONNECTED
        job.cancel()
    }

    @Synchronized
    fun connected(socket: BluetoothSocket, type: ConnectionType) {

        cancelConnections()

        connectionType = type
        currentSocket = socket

        cancelAccept()

        val transferEventsListener = object : DataTransferThread.TransferEventsListener {

            override fun onMessageReceived(message: String) {
                launch {
                    this@ConnectionController.onMessageReceived(message)
                }
            }

            override fun onMessageSent(message: String) {
                launch {
                    this@ConnectionController.onMessageSent(message)
                }
            }

            override fun onMessageSendingFailed() {
                launch {
                    this@ConnectionController.onMessageSendingFailed()
                }
            }

            override fun onConnectionPrepared(type: ConnectionType) {

                onNewForegroundMessage?.invoke(
                    application.getString(
                        R.string.notification__connected_to, socket.remoteDevice.name
                            ?: "?"
                    )
                )
                connectionState = ConnectionState.PENDING

                if (type == ConnectionType.OUTCOMING) {
                    contract.createConnectMessage("").let { message ->
                        dataTransferThread?.write(message.getDecodedMessage())
                    }
                }
            }

            override fun onConnectionCanceled() {
                currentSocket = null
                contract.reset()
            }

            override fun onConnectionLost() {
                connectionLost()
            }
        }

        val eventsStrategy = TransferEventStrategy()

        dataTransferThread =
            object : DataTransferThread(
                socket,
                type,
                transferEventsListener,
                eventsStrategy
            ) {
                override fun shouldRun(): Boolean {
                    return isConnectedOrPending()
                }
            }
        dataTransferThread?.prepare()
        dataTransferThread?.start()

        launch { subject.handleConnected(socket.remoteDevice) }
    }

    fun getCurrentContract() = contract

    fun isConnected() = connectionState == ConnectionState.CONNECTED

    fun isPending() = connectionState == ConnectionState.PENDING

    fun isConnectedOrPending() = isConnected() || isPending()

    fun replyFromNotification(text: String) {
        contract.createChatMessage(text).let { message ->
            justRepliedFromNotification = true
            sendMessage(message)
        }
    }

    fun sendMessage(message: Message) {

        if (isConnectedOrPending()) {

            val disconnect =
                message.type == Contract.MessageType.CONNECTION_REQUEST && !message.flag

            dataTransferThread?.write(message.getDecodedMessage(), disconnect)

            if (disconnect) {
                dataTransferThread?.cancel(disconnect)
                dataTransferThread = null
                prepareForAccept()
            }
        }

        if (message.type == Contract.MessageType.CONNECTION_RESPONSE) {
            if (message.flag) {
                connectionState = ConnectionState.CONNECTED
            } else {
                disconnect()
            }
        }
    }

    fun approveConnection() {
        sendMessage(
            contract.createAcceptConnectionMessage("Accept")
        )
    }

    fun rejectConnection() {
        sendMessage(
            contract.createRejectConnectionMessage("Reject")
        )
    }

    private fun onMessageSent(messageBody: String) = currentSocket?.let { socket ->

        val device = socket.remoteDevice
        val message = Message(messageBody)
        val sentMessage = messageBody

        if (message.type == Contract.MessageType.MESSAGE) {

            launch(bgContext) {

                shallowHistory.add(
                    NotificationCompat.MessagingStyle.Message(sentMessage, 0, me)
                )

                launch(uiContext) { subject.handleMessageSent(sentMessage) }
            }
        }
    }

    private fun onMessageReceived(messageBody: String) {

        val message = Message(messageBody)

        if (message.type == Contract.MessageType.MESSAGE && currentSocket != null) {

            handleReceivedMessage(message.uid, message.body)

        } else if (message.type == Contract.MessageType.DELIVERY) {

            if (message.flag) {
                subject.handleMessageDelivered(message.uid)
            } else {
                subject.handleMessageNotDelivered(message.uid)
            }
        } else if (message.type == Contract.MessageType.SEEING) {

            if (message.flag) {
                subject.handleMessageSeen(message.uid)
            }
        } else if (message.type == Contract.MessageType.CONNECTION_RESPONSE) {

            if (message.flag) {
                handleConnectionApproval(message)
            } else {
                connectionState = ConnectionState.REJECTED
                prepareForAccept()
                subject.handleConnectionRejected()
            }
        } else if (message.type == Contract.MessageType.CONNECTION_REQUEST && currentSocket != null) {

            if (message.flag) {
                handleConnectionRequest(message)
            } else {
                disconnect()
                subject.handleDisconnected()
            }
        }
    }

    private fun onMessageSendingFailed() {
        subject.handleMessageSendingFailed()
    }

    private fun handleReceivedMessage(uid: Long, text: String) = currentSocket?.let { socket ->

        val device: BluetoothDevice = socket.remoteDevice

        val receivedMessage = text

        val partner = Person.Builder().setName("?").build()
        shallowHistory.add(
            NotificationCompat.MessagingStyle.Message(
                receivedMessage, 0, partner
            )
        )

        launch(bgContext) {
            launch(uiContext) { subject.handleMessageReceived(receivedMessage) }
        }
    }

    private fun handleConnectionRequest(message: Message) = currentSocket?.let { socket ->

        val device: BluetoothDevice = socket.remoteDevice

        val parts = message.body.split(Contract.DIVIDER)

        contract setupWith if (parts.size >= 3) parts[2].trim().toInt() else 0


        view.showConnectionRequestNotification("", device.address, false)

    }

    private fun handleConnectionApproval(message: Message) = currentSocket?.let { socket ->

        val device: BluetoothDevice = socket.remoteDevice

        val parts = message.body.split(Contract.DIVIDER)

        contract setupWith if (parts.size >= 3) parts[2].trim().toInt() else 0

        connectionState = ConnectionState.CONNECTED
        subject.handleConnectionAccepted()
    }

    private fun connectionLost() {

        currentSocket = null
        contract.reset()
        if (isConnectedOrPending()) {
            launch {
                if (isPending() && connectionType == ConnectionType.INCOMING) {
                    connectionState = ConnectionState.NOT_CONNECTED
                    subject.handleConnectionWithdrawn()
                } else {
                    connectionState = ConnectionState.NOT_CONNECTED
                    subject.handleConnectionLost()
                }
                prepareForAccept()
            }
        } else {
            prepareForAccept()
        }
    }

    private inner class AcceptJob : Thread() {

        private var serverSocket: BluetoothServerSocket? = null

        init {
            try {
                serverSocket = BluetoothAdapter.getDefaultAdapter()
                    ?.listenUsingRfcommWithServiceRecord(blAppName, blAppUUID)
            } catch (e: IOException) {
                e.printStackTrace()
            }

            connectionState = ConnectionState.LISTENING
        }

        override fun run() {

            while (!isConnectedOrPending()) {

                try {
                    serverSocket?.accept()?.let { socket ->
                        when (connectionState) {
                            ConnectionState.LISTENING, ConnectionState.CONNECTING -> {
                                connected(socket, ConnectionType.INCOMING)
                            }
                            ConnectionState.NOT_CONNECTED, ConnectionState.CONNECTED, ConnectionState.PENDING, ConnectionState.REJECTED -> try {
                                socket.close()
                            } catch (e: IOException) {
                                e.printStackTrace()
                            }
                        }
                    }
                } catch (e: IOException) {
                    e.printStackTrace()
                    break
                }
            }
        }

        fun cancel() {
            try {
                serverSocket?.close()
            } catch (e: IOException) {
                e.printStackTrace()
            }
        }
    }

    private inner class ConnectJob(private val bluetoothDevice: BluetoothDevice) : Thread() {

        private var socket: BluetoothSocket? = null

        override fun run() {

            try {
                socket = bluetoothDevice.createRfcommSocketToServiceRecord(blAppUUID)
            } catch (e: IOException) {
                e.printStackTrace()
            }
            connectionState = ConnectionState.CONNECTING

            try {
                socket?.connect()
            } catch (connectException: IOException) {
                connectException.printStackTrace()
                try {
                    socket?.close()
                } catch (e: IOException) {
                    e.printStackTrace()
                }
                connectionFailed()
                return
            }

            synchronized(this@ConnectionController) {
                connectThread = null
            }

            socket?.let {
                connected(it, ConnectionType.OUTCOMING)
            }
        }

        fun cancel() {
            try {
                socket?.close()
            } catch (e: IOException) {
                e.printStackTrace()
            }
        }
    }
}

