package com.realwear.bluetooth.service.connection

enum class ConnectionState {
    CONNECTED, CONNECTING, NOT_CONNECTED, REJECTED, PENDING, LISTENING
}
