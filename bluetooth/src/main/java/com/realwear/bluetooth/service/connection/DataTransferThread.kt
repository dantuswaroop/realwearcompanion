package com.realwear.bluetooth.service.connection

import android.bluetooth.BluetoothSocket
import java.io.IOException
import java.io.InputStream
import java.io.OutputStream

abstract class DataTransferThread(
    private val socket: BluetoothSocket,
    private val type: ConnectionType,
    private val transferListener: TransferEventsListener,
    private val eventsStrategy: EventsStrategy
) : Thread() {

    private var inputStream: InputStream? = null
    private var outputStream: OutputStream? = null

    private val bufferSize = 2048
    private val buffer = ByteArray(bufferSize)

    private var skipEvents = false

    @Volatile
    private var isConnectionPrepared = false

    fun prepare() {

        try {
            inputStream = socket.inputStream
            outputStream = socket.outputStream
            isConnectionPrepared = true
            transferListener.onConnectionPrepared(type)
        } catch (e: IOException) {
            e.printStackTrace()
        }
    }

    abstract fun shouldRun(): Boolean

    override fun start() {
        require(isConnectionPrepared) { "Connection is not prepared yet." }
        super.start()
    }

    override fun run() {
        while (shouldRun()) {
            try {
                readString()?.let { message ->
                    transferListener.onMessageReceived(message)
                }
            } catch (e: IOException) {
                e.printStackTrace()
                if (!skipEvents) {
                    transferListener.onConnectionLost()
                    skipEvents = false
                }
                break
            }
        }
    }

    private fun readString(): String? {

        return inputStream?.read(buffer)?.let {
            try {
                String(buffer, 0, it)
            } catch (e: StringIndexOutOfBoundsException) {
                null
            }
        }
    }

    fun write(message: String) {
        write(message, false)
    }

    fun write(message: String, skipEvents: Boolean) {
        this.skipEvents = skipEvents
        try {
            outputStream?.write(message.toByteArray(Charsets.UTF_8))
            outputStream?.flush()
            transferListener.onMessageSent(message)
        } catch (e: Exception) {
            transferListener.onMessageSendingFailed()
            e.printStackTrace()
        }
    }

    fun cancel() {
        cancel(false)
    }

    fun cancel(skipEvents: Boolean) {
        this.skipEvents = skipEvents
        try {
            socket.close()
            isConnectionPrepared = false
            transferListener.onConnectionCanceled()
        } catch (e: IOException) {
            e.printStackTrace()
        }
    }

    interface TransferEventsListener {

        fun onMessageReceived(message: String)
        fun onMessageSent(message: String)
        fun onMessageSendingFailed()

        fun onConnectionPrepared(type: ConnectionType)

        fun onConnectionCanceled()
        fun onConnectionLost()
    }

    interface EventsStrategy {
        fun getCancellationMessage(byPartner: Boolean): String
    }

    data class FileInfo(val uid: Long, val name: String, val size: Long)
    data class CancelInfo(val byPartner: Boolean)
}
