package com.realwear.bluetooth.service.connection

import com.realwear.bluetooth.service.message.Contract
import com.realwear.bluetooth.utils.isNumber

class TransferEventStrategy : DataTransferThread.EventsStrategy {

    private val divider = Contract.DIVIDER
    override fun getCancellationMessage(byPartner: Boolean) = "8${divider}0$divider${if (byPartner) "1" else "0"}$divider"
}
