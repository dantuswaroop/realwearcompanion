package com.realwear.bluetooth.utils

data class Size(val width: Int, val height: Int)
