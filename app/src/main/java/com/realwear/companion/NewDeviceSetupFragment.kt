package com.realwear.companion

import android.annotation.SuppressLint
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.webkit.WebViewClient
import com.realwear.companion.databinding.FragmentNewDeviceSetupBinding
import kotlinx.android.synthetic.main.fragment_new_device_setup.*

class NewDeviceSetupFragment : Fragment() {

    private var binding: FragmentNewDeviceSetupBinding? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    @SuppressLint("SetJavaScriptEnabled")
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        binding = FragmentNewDeviceSetupBinding.inflate(inflater, container, false)
        val view = inflater.inflate(R.layout.fragment_new_device_setup, container, false)
        // WebViewClient allows you to handle
        // onPageFinished and override Url loading.
        binding!!.webView.webViewClient = WebViewClient()

        // this will load the url of the website
        binding!!.webView.loadUrl("https://dev.setupmyhmt.com/")

        // this will enable the javascript settings
        binding!!.webView.settings.javaScriptEnabled = true

        // if you want to enable zoom feature
        binding!!.webView.settings.setSupportZoom(true)
        return binding?.root
    }

    companion object {
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @param param1 Parameter 1.
         * @param param2 Parameter 2.
         * @return A new instance of fragment NewDeviceSetupFragment.
         */
        // TODO: Rename and change types and number of parameters
        @JvmStatic
        fun newInstance(param1: String, param2: String) =
            NewDeviceSetupFragment().apply {
            }
    }
}