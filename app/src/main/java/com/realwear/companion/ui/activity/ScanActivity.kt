package com.realwear.companion.ui.activity

import android.Manifest
import android.app.Activity
import android.bluetooth.BluetoothAdapter
import android.bluetooth.BluetoothDevice
import android.content.ActivityNotFoundException
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.m.myblechatsample.ui.activity.SkeletonActivity
import com.m.myblechatsample.ui.presenter.ScanPresenter
import com.m.realwearlibrary.control.AppKeys
import com.peel.prefs.SharedPrefs
import com.realwear.companion.R
import com.realwear.companion.ui.adapter.DevicesAdapter
import com.realwear.companion.ui.view.ScanView
import com.realwear.companion.ui.widget.ExpiringProgressBar
import org.koin.android.ext.android.inject
import org.koin.core.parameter.parametersOf

class ScanActivity : SkeletonActivity(), ScanView {

    private val presenter: ScanPresenter by inject { parametersOf(this) }

    private val container: View by bind(R.id.fl_container)
    private val turnOnHolder: View by bind(R.id.ll_turn_on)
    private val listHolder: View by bind(R.id.cl_list)
    private val progress: View by bind(R.id.fl_progress)

    private val discoveryLabel: TextView by bind(R.id.tv_discovery_label)
    private val progressBar: ExpiringProgressBar by bind(R.id.epb_progress)
    private val makeDiscoverableButton: Button by bind(R.id.btn_make_discoverable)
    private val scanForDevicesButton: Button by bind(R.id.btn_scan)

    private val pairedDevicesList: RecyclerView by bind(R.id.rv_paired_devices)

    private val devicesAdapter: DevicesAdapter = DevicesAdapter(this)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_scan, ActivityType.CHILD_ACTIVITY)
        lifecycle.addObserver(presenter)

        pairedDevicesList.layoutManager = LinearLayoutManager(this)
        pairedDevicesList.adapter = devicesAdapter

        devicesAdapter.listener = {
            presenter.onDevicePicked(it.address)
            progress.visibility = View.VISIBLE
        }

        presenter.checkBluetoothAvailability()

        findViewById<Button>(R.id.btn_turn_on).setOnClickListener {
            presenter.turnOnBluetooth()
        }

        makeDiscoverableButton.setOnClickListener {
            presenter.makeDiscoverable()
        }

        scanForDevicesButton.setOnClickListener {

            if (ContextCompat.checkSelfPermission(
                    this,
                    Manifest.permission.ACCESS_COARSE_LOCATION
                ) == PackageManager.PERMISSION_GRANTED
            ) {
                presenter.scanForDevices()
            } else {
                if (ActivityCompat.shouldShowRequestPermissionRationale(
                        this,
                        Manifest.permission.ACCESS_COARSE_LOCATION
                    )
                ) {
                    explainAskingLocationPermission()
                } else {
                    ActivityCompat.requestPermissions(
                        this,
                        arrayOf(
                            Manifest.permission.ACCESS_COARSE_LOCATION,
                            Manifest.permission.ACCESS_FINE_LOCATION
                        ), REQUEST_LOCATION_PERMISSION
                    )
                }
            }
        }
    }

    override fun openKeyboard(device: BluetoothDevice) {
//        val intent: Intent = Intent().putExtra(EXTRA_BLUETOOTH_DEVICE, device)
//        setResult(Activity.RESULT_OK, intent)
//        finish()
        progress.visibility = View.GONE
        val intent: Intent = Intent().apply {
            putExtra(EXTRA_BLUETOOTH_DEVICE, device)
            setClass(this@ScanActivity, KeyboardActivity::class.java)
        }
        startActivity(intent)
    }

    override fun showPairedDevices(pairedDevices: List<BluetoothDevice>) {

        turnOnHolder.visibility = View.GONE
        listHolder.visibility = View.VISIBLE

        if (pairedDevices.isNotEmpty()) {
            devicesAdapter.pairedList = ArrayList(pairedDevices)
            devicesAdapter.notifyDataSetChanged()
        }
    }

    override fun showBluetoothScanner() {
        container.visibility = View.VISIBLE
        presenter.checkBluetoothEnabling()
    }

    override fun showBluetoothEnablingRequest() {
        turnOnHolder.visibility = View.VISIBLE
        listHolder.visibility = View.GONE
    }

    override fun requestBluetoothEnabling() {
        val enableBtIntent = Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE)
        try {
            startActivityForResult(enableBtIntent, REQUEST_ENABLE_BLUETOOTH)
        } catch (e: ActivityNotFoundException) {
            showUnableToActivateBluetoothMessage()
        }
    }

    private fun showUnableToActivateBluetoothMessage() = doIfStarted {
        AlertDialog.Builder(this)
            .setMessage(R.string.scan__unable_to_activate)
            .setPositiveButton(R.string.general__ok, null)
            .show()
    }

    override fun showBluetoothIsNotAvailableMessage() = doIfStarted {
        AlertDialog.Builder(this)
            .setMessage(R.string.scan__no_access_to_bluetooth)
            .setPositiveButton(R.string.general__ok) { _, _ -> finish() }
            .show()
    }

    override fun showBluetoothEnablingFailed() = doIfStarted {
        AlertDialog.Builder(this)
            .setMessage(R.string.scan__bluetooth_disabled)
            .setPositiveButton(R.string.general__ok, null)
            .show()
    }

    override fun requestMakingDiscoverable() {
        val discoverableIntent = Intent(BluetoothAdapter.ACTION_REQUEST_DISCOVERABLE)
        discoverableIntent.putExtra(BluetoothAdapter.EXTRA_DISCOVERABLE_DURATION, 60)
        try {
            startActivityForResult(discoverableIntent, REQUEST_MAKE_DISCOVERABLE)
        } catch (e: ActivityNotFoundException) {
            Toast.makeText(
                this,
                getString(R.string.scan__no_discoverable_activity),
                Toast.LENGTH_LONG
            ).show()
        }
    }

    override fun showDiscoverableProcess() {
        makeDiscoverableButton.text = getString(R.string.scan__discoverable)
        makeDiscoverableButton.isEnabled = false
    }

    override fun showDiscoverableFinished() {
        makeDiscoverableButton.text = getString(R.string.scan__make_discoverable)
        makeDiscoverableButton.isEnabled = true
    }

    override fun showScanningStarted(seconds: Int) {
        progressBar.runExpiring(seconds)
        progressBar.visibility = View.VISIBLE
        discoveryLabel.visibility = View.VISIBLE
        scanForDevicesButton.text = getString(R.string.scan__stop_scanning)
    }

    override fun showScanningStopped() {
        progressBar.cancel()
        progressBar.visibility = View.GONE
        discoveryLabel.visibility = View.GONE
        scanForDevicesButton.text = getString(R.string.scan__scan_for_devices)
    }

    override fun showBluetoothDiscoverableFailure() = doIfStarted {
        AlertDialog.Builder(this)
            .setMessage(R.string.scan__unable_to_make_discoverable)
            .setPositiveButton(R.string.general__ok, null)
            .show()
    }

    override fun showServiceUnavailable() {
        progress.visibility = View.GONE
        doIfStarted {
            AlertDialog.Builder(this)
                .setMessage(R.string.scan__unable_to_connect_service)
                .setPositiveButton(R.string.general__ok, null)
                .show()
        }
    }

    override fun showUnableToConnect() {
        progress.visibility = View.GONE
        doIfStarted {
            AlertDialog.Builder(this)
                .setMessage(R.string.scan__unable_to_connect)
                .setPositiveButton(R.string.general__ok, null)
                .show()
        }
    }

    override fun addFoundDevice(device: BluetoothDevice) {
        devicesAdapter.addNewFoundDevice(device)
        devicesAdapter.notifyDataSetChanged()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (requestCode == REQUEST_ENABLE_BLUETOOTH) {
            if (resultCode == Activity.RESULT_OK) {
                presenter.onPairedDevicesReady()
            } else {
                presenter.onBluetoothEnablingFailed()
            }
        } else if (requestCode == REQUEST_MAKE_DISCOVERABLE) {
            if (resultCode > 0) {
                presenter.onMadeDiscoverable()
            }
        }
    }

    override fun onResume() {
        super.onResume()
        checkAndConnect()
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<String>,
        grantResults: IntArray
    ) {

        if (requestCode == REQUEST_LOCATION_PERMISSION) {
            if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                presenter.scanForDevices()
            } else {
                explainAskingLocationPermission()
            }
        } else if (requestCode == REQUEST_STORAGE_PERMISSION) {
            if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
//                presenter.shareApk()
            } else {
                explainAskingStoragePermission()
            }
        }
    }

    private fun explainAskingLocationPermission() {
        AlertDialog.Builder(this)
            .setMessage(R.string.scan__permission_explanation_location)
            .setPositiveButton(R.string.general__ok) { _, _ ->
                ActivityCompat.requestPermissions(
                    this,
                    arrayOf(Manifest.permission.ACCESS_COARSE_LOCATION), REQUEST_LOCATION_PERMISSION
                )
            }
            .show()
    }

    private fun explainAskingStoragePermission() {
        AlertDialog.Builder(this)
            .setMessage(R.string.scan__permission_explanation_storage)
            .setPositiveButton(R.string.general__ok) { _, _ ->
                ActivityCompat.requestPermissions(
                    this,
                    arrayOf(Manifest.permission.WRITE_EXTERNAL_STORAGE), REQUEST_STORAGE_PERMISSION
                )
            }
            .show()
    }

    companion object {

        const val EXTRA_BLUETOOTH_DEVICE = "extra.bluetooth_device"

        private const val REQUEST_ENABLE_BLUETOOTH = 101
        private const val REQUEST_MAKE_DISCOVERABLE = 102
        private const val REQUEST_LOCATION_PERMISSION = 103
        private const val REQUEST_STORAGE_PERMISSION = 104

        fun start(context: Context) =
            context.startActivity(Intent(context, ScanActivity::class.java))

        fun startForResult(context: Activity, requestCode: Int) =
            context.startActivityForResult(
                Intent(context, ScanActivity::class.java), requestCode
            )
    }

    fun checkAndConnect() {
        val lastConnectedBT = SharedPrefs.get(AppKeys.CONNECTED_BT_ID)
        if (lastConnectedBT != null &&
            lastConnectedBT.isNotEmpty()
        ) {
            val bondedDevices = BluetoothAdapter.getDefaultAdapter().bondedDevices
            bondedDevices.forEach {
                if (it.address.equals(lastConnectedBT)) {
                    presenter.onDevicePicked(lastConnectedBT)
                    return@forEach
                }
            }
        }
    }
}
