package com.realwear.companion.ui.view

interface BTKeyboardView {

    fun showBluetoothDisabled()
    fun showReceivedMessage(message: String)
    fun showSentMessage(message: String)
    fun goBack()


}