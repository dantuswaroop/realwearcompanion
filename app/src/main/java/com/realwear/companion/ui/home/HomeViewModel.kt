package com.realwear.companion.ui.home

import android.app.Application
import android.bluetooth.BluetoothAdapter
import android.bluetooth.BluetoothDevice
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.m.realwearlibrary.bluetooth.BluetoothCallBackListener
import com.m.realwearlibrary.bluetooth.BluetoothService
import com.m.realwearlibrary.control.AppKeys
import com.peel.prefs.SharedPrefs

class HomeViewModel(application: Application) : AndroidViewModel(application),
    BluetoothCallBackListener {

    private var btService: BluetoothService =
       BluetoothService(this)

    init {
        btService.start()
    }

    fun getPairedDevices(): Set<BluetoothDevice> {
        return btService.getPairedDevices();
    }

    private val _text = MutableLiveData<String>().apply {
        value = "This is home Fragment"
    }
    val text: LiveData<String> = _text

    override fun onStateChanged(state: Int) {
    }

    override fun onDeviceConnected(deviceName: String, deviceAddress: String) {
//        connectedToDevice.postValue(true)
    }

    override fun onDisconnected() {
    }

    override fun onConnectionFailed() {
//        connectedToDevice.postValue(false)
    }

    override fun onMessageReceived(byteCount: Int, bytes: ByteArray) {
    }

    override fun onMessageSent(bytes: ByteArray) {
    }

    override fun onPairedDevices(pairedDevices: List<BluetoothDevice>) {

    }

    fun checkAndConnect() : LiveData<BluetoothDevice> {
        val bluetoothDevice  = MutableLiveData<BluetoothDevice>()
        val lastConnectedBT = SharedPrefs.get(AppKeys.CONNECTED_BT_ID)
        if(lastConnectedBT != null &&
                lastConnectedBT.isNotEmpty()) {
            val bondedDevices = BluetoothAdapter.getDefaultAdapter().bondedDevices
            bondedDevices.forEach {
                if (it.address.equals(lastConnectedBT)) {
                    bluetoothDevice.postValue(it)
                    return@forEach
                }
            }
        }
        return bluetoothDevice
    }


}