package com.realwear.companion.ui.activity

import android.bluetooth.BluetoothDevice
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.widget.EditText
import android.widget.Toast
import com.m.myblechatsample.ui.activity.SkeletonActivity
import com.m.myblechatsample.ui.presenter.KeyboardPresenter
import com.realwear.companion.ui.view.BTKeyboardView
import com.m.realwearlibrary.control.json.Json
import com.m.realwearlibrary.models.BTKeyboardEvent
import com.m.realwearlibrary.models.KeyboardEvent
import com.realwear.companion.R
import kotlinx.android.synthetic.main.keyboard_activity.*
import org.koin.android.ext.android.inject
import org.koin.core.parameter.parametersOf
import timber.log.Timber


class KeyboardActivity : SkeletonActivity(), BTKeyboardView {

    private var qwertyField: EditText? = null
    val keyboardPresenter: KeyboardPresenter by inject {
        parametersOf(getBluetoothDevice(), this)
    }

    private fun getBluetoothDevice(): String? {
        return intent.extras?.getParcelable<BluetoothDevice>(ScanActivity.EXTRA_BLUETOOTH_DEVICE)?.address
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.keyboard_activity)
        lifecycle.addObserver(keyboardPresenter)

        send_button.setOnClickListener {
            keyboardPresenter.sendTextMessage(input_text.text.toString());
        }

        qwertyField = findViewById(R.id.input_text)

        qwertyField!!.setOnEditorActionListener { v, actionId, event ->
            if (event != null) {
                val command =
                    Json.gson().toJson(BTKeyboardEvent(KeyboardEvent.KeyCode, "", event.keyCode))
                keyboardPresenter.sendTextMessage(command)
            } else {
                val command =
                    Json.gson().toJson(BTKeyboardEvent(KeyboardEvent.KeyCode, "", value = actionId))
                keyboardPresenter.sendTextMessage(command)
            }
            true
        }
        qwertyField!!.addTextChangedListener(textWatcher)

    }

    private val textWatcher = object : TextWatcher {
        var initialChar: String? = null
        var initCursorPosition = 0

        override fun afterTextChanged(s: Editable?) {
            Timber.v("After Text Change %s ", s.toString())
            val command = Json.gson().toJson(BTKeyboardEvent(KeyboardEvent.Text, s.toString(), 0))
            keyboardPresenter.sendTextMessage(command)
        }

        override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            Timber.v("beforeTextChanged %s ", s.toString())
            initialChar = s.toString()
            initCursorPosition = qwertyField!!.getSelectionStart();
        }

        override fun onTextChanged(
            charSequence: CharSequence?,
            start: Int,
            before: Int,
            count: Int
        ) {
            Timber.v("onTextChanged %s ", charSequence.toString())
            val addedChar: String
            val finalCursorPosition: Int = qwertyField!!.selectionStart
            if (finalCursorPosition - initCursorPosition > 0) {
                addedChar = charSequence!![finalCursorPosition - 1].toString()
                Timber.d("onTextChanged added: $addedChar")
//                viewModel.sendText(addedChar)
            } else {
//                val delChar = initialChar!![initCursorPosition - 1]
//                Timber.d("onTextChanged deletedChar: $delChar")
                //deleted char
//                viewModel.sendText(delChar.toString())
            }
        }

    }

    override fun showBluetoothDisabled() {
        Toast.makeText(this, "Bluetooth is disabled", Toast.LENGTH_SHORT).show()
    }

    override fun showReceivedMessage(message: String) {
//        Toast.makeText(this, "Message Received " + message, Toast.LENGTH_SHORT).show()
        var receivedMessage = message
        try {
            receivedMessage = receivedMessage.substring(message.indexOf('{'))
            val command = Json.gson().fromJson(receivedMessage, BTKeyboardEvent::class.java)
            when (command.event) {
                KeyboardEvent.InputType -> {
                    qwertyField?.inputType = command.value
                    qwertyField?.imeOptions = command.imeOptions
                    qwertyField?.requestFocus()
                    qwertyField?.setText(command.text)
                }
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    override fun showSentMessage(message: String) {
//        Toast.makeText(this, "Message sent :: " + message, Toast.LENGTH_SHORT).show()
    }

    override fun goBack() {
        finish()
    }

}
