package com.realwear.companion.ui.home

import android.app.Activity
import android.bluetooth.BluetoothAdapter
import android.bluetooth.BluetoothDevice
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.realwear.companion.ui.activity.ScanActivity
import com.realwear.companion.R
import timber.log.Timber

class HomeFragment : Fragment() {

    private lateinit var pairedDevicesRecycler: RecyclerView
    private lateinit var noDevicesFoundGuide: TextView
    private var mBluetoothAdapter: BluetoothAdapter? = null
    private lateinit var homeViewModel: HomeViewModel
    private val REQUEST_ENABLE_BT = 3

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        startActivity(Intent(requireContext(), ScanActivity::class.java))
        return

       /* mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter()

        if (mBluetoothAdapter == null) {
            val activity: FragmentActivity? = activity
            Toast.makeText(activity, "Bluetooth is not available", Toast.LENGTH_LONG).show()
            activity?.finish()
        }*/
    }


    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        homeViewModel =
            ViewModelProvider(this).get(HomeViewModel::class.java)
        val root = inflater.inflate(R.layout.fragment_home, container, false)
        noDevicesFoundGuide = root.findViewById(R.id.no_paired_devices)
        pairedDevicesRecycler = root.findViewById(R.id.paired_hmt_devices)
        val button = root.findViewById(R.id.scan_devices) as Button
        button.setOnClickListener {
            val actionNavigationHomeToAddNewBTDevice = HomeFragmentDirections.actionNavigationHomeToAddNewBTDevice()
            findNavController().navigate(actionNavigationHomeToAddNewBTDevice)
        }
        return root
    }

    override fun onResume() {
        super.onResume()
        showPairedDevices()
        homeViewModel.checkAndConnect().observe(viewLifecycleOwner, {
            val action = HomeFragmentDirections.actionNavigationHomeToRemoteKeyboardFragment(it)
            findNavController().navigate(action)
        })
    }


    override fun onStart() {
        super.onStart()
        // If BT is not on, request that it be enabled.
        // setupChat() will then be called during onActivityResult
        if (!mBluetoothAdapter!!.isEnabled) {
            val enableIntent = Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE)
            startActivityForResult(
                enableIntent,
                REQUEST_ENABLE_BT
            )
            // Otherwise, setup the chat session
        } else {
            showPairedDevices()
        }
    }

    private fun showPairedDevices() {
        val pairedDevices = homeViewModel.getPairedDevices()
        if (pairedDevices.isNotEmpty()) {
            pairedDevicesRecycler.visibility = View.VISIBLE
            noDevicesFoundGuide.visibility = View.GONE
            val linearLayoutManager = LinearLayoutManager(requireActivity())
            linearLayoutManager.orientation = LinearLayoutManager.VERTICAL
            pairedDevicesRecycler.layoutManager = linearLayoutManager

            pairedDevicesRecycler.adapter =
                PairedDevicesRecyclerAdapter(requireContext(), pairedDevices.toList())
        } else {
            pairedDevicesRecycler.visibility = View.GONE
            noDevicesFoundGuide.visibility = View.VISIBLE
        }
    }

    inner class PairedDevicesRecyclerAdapter(
        val context: Context,
        val pairedDevices: List<BluetoothDevice>
    ) :
        RecyclerView.Adapter<PairedDevicesRecyclerAdapter.PairedDevicesHolder>() {

        inner class PairedDevicesHolder(v: View) : RecyclerView.ViewHolder(v) {
            var deviceName: TextView

            init {
                deviceName = v.findViewById<TextView>(R.id.paried_hmt_device_name)
            }
        }

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PairedDevicesHolder {
            return PairedDevicesHolder(
                LayoutInflater.from(context).inflate(R.layout.bt_paired_device_row, parent, false)
            )
        }

        override fun onBindViewHolder(holder: PairedDevicesHolder, position: Int) {
            val device = pairedDevices.get(position)
            holder.deviceName.text = device.name

            holder.itemView.setOnClickListener {
                val actionNavigationHomeToRemoteKeyboardFragment =
                    HomeFragmentDirections.actionNavigationHomeToRemoteKeyboardFragment(device)
                findNavController().navigate(actionNavigationHomeToRemoteKeyboardFragment)
            }
        }

        override fun getItemCount(): Int {
            return pairedDevices.size
        }

    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        when (requestCode) {
            REQUEST_ENABLE_BT -> {
                if (resultCode == Activity.RESULT_OK) {
                    //enabled bluetooth
                    showPairedDevices()
                } else {
                    // User did not enable Bluetooth or an error occurred
                    Timber.d("BT not enabled")
                    Toast.makeText(
                        activity, "Bluetooth is not enabled, Need Bluetooth connect to proceed",
                        Toast.LENGTH_SHORT
                    ).show()
                    requireActivity().finish()
                }
            }
        }

    }


}