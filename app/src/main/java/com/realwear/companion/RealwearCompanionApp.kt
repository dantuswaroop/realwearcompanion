package com.realwear.companion

import android.app.Application
import com.google.gson.Gson
import com.m.myblechatsample.di.applicationModule
import com.m.myblechatsample.di.viewModule
import com.m.realwearlibrary.control.Statics
import com.peel.prefs.SharedPrefs
import com.realwear.bluetooth.di.bluetoothApplicationModule
import com.realwear.bluetooth.di.bluetoothConnectionModule
import com.realwear.companion.utils.RealwearCompanionLogTree
import org.koin.android.ext.android.startKoin
import timber.log.Timber

class RealwearCompanionApp : Application() {

    override fun onCreate() {
        super.onCreate()
        Statics.APP = this
        SharedPrefs.init(this, Gson())
        Timber.plant(RealwearCompanionLogTree.getInstance())

        startKoin(
            this, listOf(
                bluetoothConnectionModule,
                bluetoothApplicationModule,
                applicationModule,
                viewModule
            )
        )
    }
}