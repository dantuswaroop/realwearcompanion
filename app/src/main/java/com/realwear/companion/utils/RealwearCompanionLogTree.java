package com.realwear.companion.utils;

import android.util.Log;

import androidx.annotation.Nullable;

import com.realwear.companion.BuildConfig;

import timber.log.Timber;

public class RealwearCompanionLogTree {
    private static final String CRASHLYTICS_KEY_PRIORITY = "priority";
    private static final String CRASHLYTICS_KEY_TAG = "tag";
    private static final String CRASHLYTICS_KEY_MESSAGE = "message";
    private static Timber.Tree tree = null;

    public static Timber.Tree getInstance() {
        if (null == tree) {
            tree = BuildConfig.DEBUG ? new DebugTree() : new ProductionTree();
        }
        return tree;
    }

    private static void logToCrashlytics(int priority, String tag, String message, Throwable t) {
        if (priority > Log.WARN) {

        }
    }

    private static String formatLog(String tag, String message) {
        return tag + "\n " + message;
    }

    private static class DebugTree extends Timber.DebugTree {
        @Override
        protected void log(int priority, @Nullable String tag, @Nullable String message, @Nullable Throwable t) {
            if (BuildConfig.DEBUG) {
                super.log(priority, tag, message, t);
            }
            logToCrashlytics(priority, tag, message, t);
        }
    }

    private static class ProductionTree extends Timber.Tree {
        @Override
        protected void log(int priority, @Nullable String tag, @Nullable String message, @Nullable Throwable t) {
            if (priority == Log.VERBOSE || priority == Log.DEBUG || priority == Log.INFO) {
                return;
            }
            logToCrashlytics(priority, tag, message, t);
        }
    }

}
