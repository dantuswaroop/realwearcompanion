package com.realwear.companion

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import com.realwear.companion.databinding.FragmentNavigationMenuBinding

class NavigationMenuFragment : Fragment() {

    private var _binding: FragmentNavigationMenuBinding? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        _binding = FragmentNavigationMenuBinding.inflate(inflater, container, false)

        _binding!!.newDeviceLabel.setOnClickListener {
            val actionNavigationMenuToNewDeviceSetupFragment =
                NavigationMenuFragmentDirections.actionNavigationMenuToSetupDevice()
            findNavController().navigate(actionNavigationMenuToNewDeviceSetupFragment)
        }

        _binding!!.remoteKeyboardLabel.setOnClickListener {
            val actionNavigationMenuToHomeFragment =
                NavigationMenuFragmentDirections.actionNavigationMenuToHomeFragment()
            findNavController().navigate(actionNavigationMenuToHomeFragment)
        }
        val view = _binding!!.root
        return view
    }

    companion object {
        @JvmStatic
        fun newInstance() =
            NavigationMenuFragment().apply {}
    }
}