package com.realwear.companion

import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.*
import android.view.inputmethod.EditorInfo
import android.widget.*
import android.widget.TextView.OnEditorActionListener
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.observe
import androidx.navigation.findNavController
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import com.m.realwearlibrary.control.AppKeys
import com.m.realwearlibrary.control.json.Json
import com.m.realwearlibrary.models.BTKeyboardEvent
import com.m.realwearlibrary.models.KeyboardEvent
import com.peel.prefs.SharedPrefs
import timber.log.Timber


class RemoteKeyboardFragment : Fragment() {

    private lateinit var progressBackground: View
    private lateinit var progressBar: ProgressBar
    val args: RemoteKeyboardFragmentArgs by navArgs()

    private lateinit var viewModel: RemoteKeyboardViewModel
    var qwertyField: EditText? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        val view = inflater.inflate(R.layout.remote_keyboard_fragment, container, false)
        val numberField: EditText = view.findViewById(R.id.testNumberField)
        qwertyField = view.findViewById(R.id.testQwertyField)

        numberField.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {

            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {

            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                val currentChar: Char? = s!!.elementAtOrNull(start + before)
                if (currentChar != null) {
                    viewModel.sendText(currentChar.toString())
                }
            }

        })

        qwertyField!!.setOnEditorActionListener { v, actionId, event ->
            if (event != null) {
                val command =
                    Json.gson().toJson(BTKeyboardEvent(KeyboardEvent.KeyCode, "", event.keyCode))
                viewModel.sendText(command)
            } else {
                val command =
                        Json.gson().toJson(BTKeyboardEvent(KeyboardEvent.KeyCode, "", value = actionId))
                viewModel.sendText(command)
            }
            true
        }
        qwertyField!!.addTextChangedListener(textWatcher)


        return view
    }

    private fun showLoading(show: Boolean) {
        if (show) {
            progressBackground.visibility = View.VISIBLE
            progressBar.visibility = View.VISIBLE
        } else {
            progressBackground.visibility = View.GONE
            progressBar.visibility = View.GONE
        }
    }

    private val textWatcher = object : TextWatcher {
        var initialChar: String? = null
        var initCursorPosition = 0

        override fun afterTextChanged(s: Editable?) {
            Timber.v("After Text Change %s ", s.toString())
            val command = Json.gson().toJson(BTKeyboardEvent(KeyboardEvent.Text, s.toString(), 0))
            viewModel.sendText(command)
        }

        override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            Timber.v("beforeTextChanged %s ", s.toString())
            initialChar = s.toString()
            initCursorPosition = qwertyField!!.getSelectionStart();
        }

        override fun onTextChanged(
            charSequence: CharSequence?,
            start: Int,
            before: Int,
            count: Int
        ) {
            Timber.v("onTextChanged %s ", charSequence.toString())
            val addedChar: String
            val finalCursorPosition: Int = qwertyField!!.selectionStart
            if (finalCursorPosition - initCursorPosition > 0) {
                addedChar = charSequence!![finalCursorPosition - 1].toString()
                Timber.d("onTextChanged added: $addedChar")
//                viewModel.sendText(addedChar)
            } else {
//                val delChar = initialChar!![initCursorPosition - 1]
//                Timber.d("onTextChanged deletedChar: $delChar")
                //deleted char
//                viewModel.sendText(delChar.toString())
            }
        }

    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        progressBackground = view.findViewById<View>(R.id.progress_background) as View
        progressBar = view.findViewById(R.id.progress_bar) as ProgressBar
        view.findViewById<TextView>(R.id.disconnect).setOnClickListener {
            viewModel.disconnect()
            view.findNavController().popBackStack()
        }

        view.findViewById<Button>(R.id.button).setOnClickListener {
            viewModel.sendBTKeyEvent(KeyEvent.KEYCODE_S)
        }
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProvider(this).get(RemoteKeyboardViewModel::class.java)
        showLoading(true)
        viewModel.connectToDevice(args.bluetoothDevice).observe(viewLifecycleOwner) {
            showLoading(false)
            if (it != null) {
                val bluetoothDevice = args.bluetoothDevice
                SharedPrefs.put(AppKeys.CONNECTED_BT_ID, bluetoothDevice?.address!!)
                Toast.makeText(
                        activity, "Bluetooth connected ? :: $it",
                        Toast.LENGTH_SHORT
                ).show()
            } else {
                // failed to connect
                SharedPrefs.remove(AppKeys.CONNECTED_BT_ID)
                Toast.makeText(
                        activity, "Failed to connect",
                        Toast.LENGTH_SHORT
                ).show()
                findNavController().popBackStack()
            }
        }

        viewModel.listenToInputType().observe(viewLifecycleOwner, {
            when(it.event) {
                KeyboardEvent.InputType -> {
                    qwertyField?.inputType = it.value
                    qwertyField?.imeOptions = it.imeOptions
                    qwertyField?.requestFocus()
                    qwertyField?.setText(it.text)
                }
            }

        })
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> findNavController().popBackStack()
        }
        return super.onOptionsItemSelected(item)
    }

}