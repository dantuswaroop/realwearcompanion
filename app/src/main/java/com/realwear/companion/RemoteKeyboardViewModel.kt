package com.realwear.companion

import android.app.Application
import android.bluetooth.BluetoothDevice
import android.widget.Toast
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.google.gson.Gson
import com.m.realwearlibrary.bluetooth.BluetoothCallBackListener
import com.m.realwearlibrary.bluetooth.BluetoothService
import com.m.realwearlibrary.models.BTKeyboardEvent

class RemoteKeyboardViewModel(application: Application) : AndroidViewModel(application) ,
    BluetoothCallBackListener {

    private var inputType : MutableLiveData<BTKeyboardEvent> = MutableLiveData()

    private var btService: BluetoothService =
        BluetoothService(this)

    init {
        btService.start()
    }

    override fun onStateChanged(state: Int) {

    }

    override fun onDeviceConnected(deviceName: String, deviceAddress: String) {
        connectedToDevice.postValue(deviceAddress)
    }

    override fun onDisconnected() {
        connectedToDevice.postValue(null)
    }

    override fun onConnectionFailed() {
        connectedToDevice.postValue(null)
    }

    override fun onMessageReceived(byteCount: Int, bytes: ByteArray) {
        val text = String(bytes, 0, byteCount)
        val fromJson = Gson().fromJson(text, BTKeyboardEvent::class.java)
        inputType.postValue(fromJson)
    }

    override fun onMessageSent(bytes: ByteArray) {
    }

    override fun onPairedDevices(pairedDevices: List<BluetoothDevice>) {
    }

    private var connectedToDevice: MutableLiveData<String> = MutableLiveData()

    fun connectToDevice(device: BluetoothDevice) : LiveData<String> {
        btService.connect(device, true)

        return connectedToDevice
    }

    fun disconnect() {
        btService.stop()
    }

    fun sendBTKeyEvent(keycode: Int) {
        btService.write(("" + keycode).toByteArray())
    }

    fun sendText(text: String) {
        btService.write(text.toByteArray())
    }

    fun listenToInputType(): LiveData<BTKeyboardEvent> {
        return inputType
    }

}