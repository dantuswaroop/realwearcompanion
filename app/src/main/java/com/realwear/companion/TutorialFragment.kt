package com.realwear.companion

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.viewpager2.widget.ViewPager2
import com.realwear.companion.ui.viewpageindicators.DotIndicatorPager2Adapter
import com.realwear.companion.ui.viewpageindicators.SpringDotsIndicator

class TutorialFragment : Fragment() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_tutorial, container, false)

        val springDotsIndicator = view.findViewById<SpringDotsIndicator>(R.id.spring_dots_indicator)

        val viewPager2 = view.findViewById<ViewPager2>(R.id.view_pager2)
        val adapter = DotIndicatorPager2Adapter()
        viewPager2.adapter = adapter
        springDotsIndicator.setViewPager2(viewPager2)
        return view
    }

}