package com.realwear.companion.view

import android.app.*
import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.os.Build
import androidx.core.app.NotificationCompat
import androidx.core.content.ContextCompat
import com.realwear.bluetooth.service.BluetoothConnectionService
import com.realwear.bluetooth.ui.view.NotificationView
import com.realwear.bluetooth.utils.getNotificationManager
import com.realwear.companion.MainActivity
import com.realwear.companion.R
import java.util.*

class NotificationViewImpl(private val context: Context) : NotificationView {

    private val random = Random()
    private val notificationManager = context.getNotificationManager()

    override fun getForegroundNotification(message: String): Notification {

        val notificationIntent = Intent(context, MainActivity::class.java).apply {
            flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
        }
        val pendingIntent = PendingIntent.getActivity(context, 0, notificationIntent, 0)

        val stopIntent = Intent(context, BluetoothConnectionService::class.java).apply {
            action = BluetoothConnectionService.ACTION_STOP
        }
        val stopPendingIntent = PendingIntent.getService(context, generateCode(), stopIntent, 0)

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val channel = NotificationChannel(NotificationView.CHANNEL_FOREGROUND,"Noti", NotificationManager.IMPORTANCE_LOW).apply {
                setShowBadge(false)
            }
            notificationManager.createNotificationChannel(channel)
        }

        val builder = NotificationCompat.Builder(context, NotificationView.CHANNEL_FOREGROUND)
                .setContentTitle(context.getString(R.string.app_name))
                .setContentText(message)
                .setSmallIcon(R.drawable.ic_launcher_background)
                .setContentIntent(pendingIntent)
                .setOngoing(true)
                .setPriority(NotificationCompat.PRIORITY_DEFAULT)
                .addAction(0, "Stop", stopPendingIntent)

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            builder.color = ContextCompat.getColor(context, R.color.black)
        }

        return builder.build()
    }

    override fun showConnectionRequestNotification(deviceName: String, address: String, soundEnabled: Boolean) {

        val notificationIntent = Intent(context, MainActivity::class.java).apply {
            flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
        }
        val pendingIntent = PendingIntent.getActivity(context, generateCode(), notificationIntent, 0)

        val approveIntent = Intent(NotificationView.ACTION_CONNECTION).apply {
            putExtra(NotificationView.EXTRA_APPROVED, true)
            putExtra(NotificationView.EXTRA_ADDRESS, address)
        }
        val approvePendingIntent = PendingIntent.getBroadcast(context, generateCode(), approveIntent, 0)
        val approveAction = NotificationCompat.Action(R.drawable.ic_start_chat, context.getString(R.string.general__start_chat), approvePendingIntent)

        val rejectIntent = Intent(NotificationView.ACTION_CONNECTION).apply {
            putExtra(NotificationView.EXTRA_APPROVED, false)
        }
        val rejectPendingIntent = PendingIntent.getBroadcast(context, generateCode(), rejectIntent, 0)
        val rejectAction = NotificationCompat.Action(R.drawable.ic_cancel, context.getString(R.string.chat__disconnect), rejectPendingIntent)

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val channel = NotificationChannel(NotificationView.CHANNEL_REQUEST, context.getString(R.string.notification__channel_request), NotificationManager.IMPORTANCE_HIGH).apply {
                setShowBadge(true)
            }
            notificationManager.createNotificationChannel(channel)
        }

        val builder = NotificationCompat.Builder(context, NotificationView.CHANNEL_REQUEST)
                .setContentTitle(context.getString(R.string.notification__connection_request))
                .setContentText(context.getString(R.string.notification__connection_request_body, deviceName))
                .setLights(Color.BLUE, 3000, 3000)
                .setSmallIcon(R.drawable.ic_connection_request)
                .setContentIntent(pendingIntent)
                .setAutoCancel(true)
                .setPriority(NotificationCompat.PRIORITY_MAX)
                .setCategory(NotificationCompat.CATEGORY_CALL)
                .addAction(approveAction)
                .addAction(rejectAction)

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            builder.color = ContextCompat.getColor(context, R.color.black)
        }

        val notification = builder.build()

        if (soundEnabled && Build.VERSION.SDK_INT < Build.VERSION_CODES.O) {
            notification.defaults = notification.defaults or Notification.DEFAULT_SOUND
        }

        notificationManager.notify(
            NotificationView.NOTIFICATION_TAG_CONNECTION,
                NotificationView.NOTIFICATION_ID_CONNECTION, notification)
    }

    private fun generateCode() = random.nextInt(10_000)
}
