package com.m.myblechatsample.di

import com.m.myblechatsample.ui.presenter.KeyboardPresenter
import com.m.myblechatsample.ui.presenter.ScanPresenter
import org.koin.dsl.module.module

val applicationModule = module {

    factory { params ->
        ScanPresenter(params[0], get(), get())
    }

    factory { params ->
        KeyboardPresenter(params[0], params[1], get(), get())
    }
}
