package com.m.myblechatsample.di

import com.realwear.bluetooth.ui.view.NotificationView
import com.realwear.companion.view.NotificationViewImpl
import org.koin.android.ext.koin.androidContext
import org.koin.dsl.module.module


val viewModule = module {
    single { NotificationViewImpl(androidContext()) as NotificationView }
}
