package com.realwear.companion

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.view.Window
import android.view.WindowManager
import com.realwear.companion.ui.activity.ScanActivity

class SplashActivity : Activity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        requestWindowFeature(Window.FEATURE_NO_TITLE)
        super.onCreate(savedInstanceState)
        window.setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
            WindowManager.LayoutParams.FLAG_FULLSCREEN)
        setContentView(R.layout.splash_layout)
        Handler().postDelayed({
            startActivity(Intent(this@SplashActivity, ScanActivity::class.java))
            finish()
        }, 3_000)
    }

}