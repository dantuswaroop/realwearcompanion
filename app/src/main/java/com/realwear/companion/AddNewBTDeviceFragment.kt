package com.realwear.companion

import android.app.AlertDialog
import android.app.ProgressDialog
import android.bluetooth.BluetoothAdapter
import android.bluetooth.BluetoothDevice
import android.content.DialogInterface
import android.content.pm.PackageManager
import android.os.Bundle
import android.os.Handler
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.ProgressBar
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.android.material.snackbar.Snackbar
import com.m.realwearlibrary.control.AppKeys
import com.peel.prefs.SharedPrefs
import com.realwear.companion.discovery.BluetoothController
import com.realwear.companion.discovery.DeviceRecyclerViewAdapter
import com.realwear.companion.discovery.ListInteractionListener
import com.realwear.companion.discovery.RecyclerViewProgressEmptySupport

class AddNewBTDeviceFragment : Fragment(), ListInteractionListener<BluetoothDevice> {

    private lateinit var rootView: View

    /**
     * Tag string used for logging.
     */
    private val TAG = "AddNewBTDeviceFragment"

    /**
     * The controller for Bluetooth functionalities.
     */
    private lateinit var bluetooth: BluetoothController

    /**
     * Progress dialog shown during the pairing process.
     */
    private var bondingProgressDialog: ProgressDialog? = null

    /**
     * Adapter for the recycler view.
     */
    private lateinit var recyclerViewAdapter: DeviceRecyclerViewAdapter

    private lateinit var recyclerView: RecyclerViewProgressEmptySupport

    private val adContainer: LinearLayout? = null


    companion object {
        fun newInstance() = AddNewBTDeviceFragment()
    }

    private lateinit var viewModel: AddNewBTDeviceViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.add_new_b_t_device_fragment, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        rootView = view;

        // Sets up the RecyclerView.
        recyclerViewAdapter = DeviceRecyclerViewAdapter(this)
        recyclerView = view.findViewById(R.id.list) as RecyclerViewProgressEmptySupport
        recyclerView!!.layoutManager = LinearLayoutManager(requireContext())

        // Sets the view to show when the dataset is empty. IMPORTANT : this method must be called
        // before recyclerView.setAdapter().

        // Sets the view to show when the dataset is empty. IMPORTANT : this method must be called
        // before recyclerView.setAdapter().
        val emptyView: View = view.findViewById(R.id.empty_list)
        recyclerView!!.setEmptyView(emptyView)

        // Sets the view to show during progress.

        // Sets the view to show during progress.
        val progressBar = view.findViewById(R.id.progressBar) as ProgressBar
        recyclerView!!.setProgressView(progressBar)

        recyclerView!!.adapter = recyclerViewAdapter

        // [#11] Ensures that the Bluetooth is available on this device before proceeding.

        // [#11] Ensures that the Bluetooth is available on this device before proceeding.
        val hasBluetooth: Boolean =
            requireActivity().getPackageManager().hasSystemFeature(PackageManager.FEATURE_BLUETOOTH)
        if (!hasBluetooth) {
            val dialog: AlertDialog = AlertDialog.Builder(requireContext()).create()
            dialog.setTitle(getString(R.string.bluetooth_not_available_title))
            dialog.setMessage(getString(R.string.bluetooth_not_available_message))
            dialog.setButton(AlertDialog.BUTTON_NEUTRAL, "OK",
                DialogInterface.OnClickListener { dialog, which -> // Closes the dialog and terminates the activity.
                    dialog.dismiss()
                    requireActivity().finish()
                })
            dialog.setCancelable(false)
            dialog.show()
        }



        Handler().postDelayed({
            // If the bluetooth is not enabled, turns it on.
            if (!bluetooth.isBluetoothEnabled) {
                Snackbar.make(view, R.string.enabling_bluetooth, Snackbar.LENGTH_SHORT).show()
                bluetooth.turnOnBluetoothAndScheduleDiscovery()
            } else {
                //Prevents the user from spamming the button and thus glitching the UI.
                if (!bluetooth.isDiscovering) {
                    // Starts the discovery.
                    Snackbar.make(view, R.string.device_discovery_started, Snackbar.LENGTH_SHORT)
                        .show()
                    bluetooth.startDiscovery()
                } else {
                    Snackbar.make(view, R.string.device_discovery_stopped, Snackbar.LENGTH_SHORT)
                        .show()
                    bluetooth.cancelDiscovery()
                }
            }
        }, 1_000)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProvider(this).get(AddNewBTDeviceViewModel::class.java)

        // Sets up the bluetooth controller.
        bluetooth =
            BluetoothController(
                requireActivity(),
                BluetoothAdapter.getDefaultAdapter(),
                recyclerViewAdapter
            )
    }

    override fun onItemClick(device: BluetoothDevice?) {
        Log.d(TAG, "Item clicked : " + BluetoothController.deviceToString(device))
        if (bluetooth.isAlreadyPaired(device)) {
            Log.d(TAG, "Device already paired!")
            Toast.makeText(requireContext(), R.string.device_already_paired, Toast.LENGTH_SHORT).show()
        } else {
            Log.d(TAG, "Device not paired. Pairing.")
            val outcome = bluetooth.pair(device)

            // Prints a message to the user.
            val deviceName = BluetoothController.getDeviceName(device)
            if (outcome) {
                // The pairing has started, shows a progress dialog.
                Log.d(TAG, "Showing pairing dialog")
                bondingProgressDialog = ProgressDialog.show(
                    requireContext(), "",
                    "Pairing with device $deviceName...", true, false
                )
            } else {
                Log.d(TAG, "Error while pairing with device $deviceName!")
                Toast.makeText(
                    requireContext(),
                    "Error while pairing with device $deviceName!",
                    Toast.LENGTH_SHORT
                ).show()
            }
        }
    }

    override fun startLoading() {
        recyclerView.startLoading()
    }

    override fun endLoading(partialResults: Boolean) {
        recyclerView.endLoading()
    }

    override fun endLoadingWithDialog(error: Boolean, device: BluetoothDevice?) {
        if (bondingProgressDialog != null) {
            val message: String
            val deviceName = BluetoothController.getDeviceName(device)

            // Gets the message to print.
            message = if (error) {
                "Failed pairing with device $deviceName!"
            } else {
                "Succesfully paired with device $deviceName!"
            }

            // Dismisses the progress dialog and prints a message to the user.
            bondingProgressDialog!!.dismiss()
            Snackbar.make(rootView, message, Snackbar.LENGTH_SHORT).show()

            // Cleans up state.
            bondingProgressDialog = null
            if(!error) {
                SharedPrefs.put(AppKeys.CONNECTED_BT_ID, device?.address!!)
                findNavController().popBackStack()
            }
        }
    }

}