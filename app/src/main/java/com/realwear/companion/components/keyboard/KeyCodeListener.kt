package com.realwear.companion.components.keyboard

interface KeyCodeListener {
    fun characterClicked(c: Int)
}