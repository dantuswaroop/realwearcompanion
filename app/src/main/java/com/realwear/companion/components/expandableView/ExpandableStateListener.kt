package com.realwear.companion.components.expandableView

interface ExpandableStateListener {
    fun onStateChange(state: ExpandableState)
}