package com.realwear.companion.components.expandableView

enum class ExpandableState {
    COLLAPSED,
    COLLAPSING,
    EXPANDED,
    EXPANDING
}