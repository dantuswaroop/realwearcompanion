package com.realwear.companion.components.keyboard

import com.realwear.companion.components.keyboard.controllers.KeyboardController

interface KeyboardListener {
    fun characterClicked(c: Char)
    fun characterClicked(c: Int)
    fun specialKeyClicked(key: KeyboardController.SpecialKey)
}