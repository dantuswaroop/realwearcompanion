package com.realwear.companion.components.keyboard

import android.content.Context
import android.graphics.Color
import android.text.InputType
import android.util.AttributeSet
import android.util.Log
import android.view.View
import android.view.View.OnFocusChangeListener
import android.view.ViewGroup
import android.widget.EditText
import android.widget.LinearLayout
import android.widget.ScrollView
import com.realwear.companion.components.expandableView.ExpandableState
import com.realwear.companion.components.expandableView.ExpandableStateListener
import com.realwear.companion.components.expandableView.ExpandableView
import com.realwear.companion.components.keyboard.controllers.DefaultKeyboardController
import com.realwear.companion.components.keyboard.controllers.KeyboardController
import com.realwear.companion.components.keyboard.controllers.NumberDecimalKeyboardController
import com.realwear.companion.components.keyboard.layouts.KeyboardLayout
import com.realwear.companion.components.keyboard.layouts.NumberDecimalKeyboardLayout
import com.realwear.companion.components.keyboard.layouts.NumberKeyboardLayout
import com.realwear.companion.components.keyboard.layouts.QwertyKeyboardLayout
import com.realwear.companion.components.textFields.CustomTextField
import com.realwear.companion.components.utilities.ComponentUtils
import java.util.*

class CustomKeyboardView(context: Context, attr: AttributeSet) : ExpandableView(context, attr) {
    private var fieldInFocus: EditText? = null
    private val keyboards = HashMap<EditText, KeyboardLayout?>()
    private val keyboardListener: KeyboardListener
    var keyCodeListener: KeyCodeListener? = null
        set(value) {
            field = value
        }

    init {
        setBackgroundColor(Color.GRAY)

        keyboardListener = object: KeyboardListener {
            override fun characterClicked(c: Char) {
                // don't need to do anything here
            }

            override fun characterClicked(c: Int) {
                Log.v("Custom", "Key code1111 $c")
                if (keyCodeListener != null) {
                    keyCodeListener!!.characterClicked(c)
                }
            }

            override fun specialKeyClicked(key: KeyboardController.SpecialKey) {
                if (key === KeyboardController.SpecialKey.DONE) {
                    translateLayout()
                } else if (key === KeyboardController.SpecialKey.NEXT) {
                    fieldInFocus?.focusSearch(View.FOCUS_DOWN)?.let {
                        it.requestFocus()
                        checkLocationOnScreen()
                        return
                    }
                }
            }
        }

        // register listener with parent (listen for state changes)
        registerListener(object: ExpandableStateListener {
            override fun onStateChange(state: ExpandableState) {
                if (state === ExpandableState.EXPANDED) {
                    checkLocationOnScreen()
                }
            }
        })

        // empty onClickListener prevents user from
        // accidentally clicking views under the keyboard
        setOnClickListener({})
        isSoundEffectsEnabled = false
//        registerEditText(CustomKeyboardView.KeyboardType.QWERTY)
    }

    fun registerEditText(type: KeyboardType) {
        updateState(ExpandableState.COLLAPSED)
        renderKeyboard(type)

        translateLayout()
    }

    fun registerEditText(type: KeyboardType, field: EditText) {
        if (!field.isEnabled) {
            return  // disabled fields do not have input connections
        }

        field.setRawInputType(InputType.TYPE_CLASS_TEXT)
        field.setTextIsSelectable(true)
        field.showSoftInputOnFocus = false
        field.isSoundEffectsEnabled = false
        field.isLongClickable = false

        keyboards[field] = createKeyboardLayout(type)
        keyboards[field]?.registerKeyboardListener(keyboardListener)

        field.onFocusChangeListener = OnFocusChangeListener { _: View, hasFocus: Boolean ->
            if (hasFocus) {
                ComponentUtils.hideSystemKeyboard(context, field)

                // if we can find a view below this field, we want to replace the
                // done button with the next button in the attached keyboard
                field.focusSearch(View.FOCUS_DOWN)?.run {
                    if (this is EditText) keyboards[field]?.hasNextFocus = true
                }
                fieldInFocus = field

                renderKeyboard()
                if (!isExpanded) {
                    translateLayout()
                }
            } else if (!hasFocus && isExpanded) {
                for (editText in keyboards.keys) {
                    if (editText.hasFocus()) {
                        return@OnFocusChangeListener
                    }
                }
                translateLayout()
            }
        }

        field.setOnClickListener({
            if (!isExpanded) {
                translateLayout()
            }
        })
    }

    fun autoRegisterEditTexts(rootView: ViewGroup) {
        registerEditTextsRecursive(rootView)
    }

    private fun registerEditTextsRecursive(view: View) {
        if (view is ViewGroup) {
            for (i in 0 until view.childCount) {
                registerEditTextsRecursive(view.getChildAt(i))
            }
        } else {
            if (view is CustomTextField) {
                registerEditText(view.keyboardType, view)
            } else if (view is EditText) {
                when (view.inputType) {
                    InputType.TYPE_CLASS_NUMBER -> {
                        registerEditText(KeyboardType.NUMBER, view)
                    }
                    InputType.TYPE_NUMBER_FLAG_DECIMAL -> {
                        registerEditText(KeyboardType.NUMBER_DECIMAL, view)
                    }
                    else -> {
                        registerEditText(KeyboardType.QWERTY, view)
                    }
                }
            }
        }
    }

    fun unregisterEditText(field: EditText?) {
        keyboards.remove(field)
    }

    fun clearEditTextCache() {
        keyboards.clear()
    }

    private fun renderKeyboard() {
        removeAllViews()
        val keyboard: KeyboardLayout? = keyboards[fieldInFocus]
        keyboard?.let {
            it.orientation = LinearLayout.VERTICAL
            it.createKeyboard(measuredWidth.toFloat())
            addView(keyboard)
        }
    }

    private fun renderKeyboard(type: KeyboardType) {
        removeAllViews()
        val keyboard: KeyboardLayout? = createKeyboardLayout(type)
        keyboard?.let {
            keyboard.registerKeyboardListener(keyboardListener)
            it.orientation = LinearLayout.VERTICAL
            it.createKeyboard(measuredWidth.toFloat())
            addView(keyboard)
        }
    }

    private fun createKeyboardLayout(type: KeyboardType): KeyboardLayout? {
        when(type) {
            KeyboardType.NUMBER -> {
                return NumberKeyboardLayout(context, createKeyboardController(type))
            }
            KeyboardType.NUMBER_DECIMAL -> {
                return NumberDecimalKeyboardLayout(context, createKeyboardController(type))
            }
            KeyboardType.QWERTY -> {
                return QwertyKeyboardLayout(context, createKeyboardController(type))
            }
            else -> return@createKeyboardLayout null // this should never happen
        }
    }

    private fun createKeyboardController(type: KeyboardType): KeyboardController? {
        return when(type) {
            KeyboardType.NUMBER_DECIMAL -> {
                NumberDecimalKeyboardController()
            }
            else -> {
                // not all keyboards require a custom controller
                DefaultKeyboardController()
            }
        }
    }

    override fun configureSelf() {
        renderKeyboard()
        checkLocationOnScreen()
    }

    /**
     * Check if fieldInFocus has a parent that is a ScrollView.
     * Ensure that ScrollView is enabled.
     * Check if the fieldInFocus is below the KeyboardLayout (measured on the screen).
     * If it is, find the deltaY between the top of the KeyboardLayout and the top of the
     * fieldInFocus, add 20dp (for padding), and scroll to the deltaY.
     * This will ensure the keyboard doesn't cover the field (if conditions above are met).
     */
    private fun checkLocationOnScreen() {
//        fieldInFocus?.run {
//            var fieldParent = this.parent
//            while (fieldParent !== null) {
//                if (fieldParent is ScrollView) {
//                    if (!fieldParent.isSmoothScrollingEnabled) {
//                        break
//                    }
//
//                    val fieldLocation = IntArray(2)
//                    this.getLocationOnScreen(fieldLocation)
//
//                    val keyboardLocation = IntArray(2)
//                    this@CustomKeyboardView.getLocationOnScreen(keyboardLocation)
//
//                    val fieldY = fieldLocation[1]
//                    val keyboardY = keyboardLocation[1]
//
//                    if (fieldY > keyboardY) {
//                        val deltaY = (fieldY - keyboardY)
//                        val scrollTo = (fieldParent.scrollY + deltaY + this.measuredHeight + 10.toDp)
//                        fieldParent.smoothScrollTo(0, scrollTo)
//                    }
//                    break
//                }
//                fieldParent = fieldParent.parent
//            }
//        }
        var fieldParent = this.parent
        while (fieldParent !== null) {
            if (fieldParent is ScrollView) {
                if (!fieldParent.isSmoothScrollingEnabled) {
                    break
                }

                val fieldLocation = IntArray(2)
                this.getLocationOnScreen(fieldLocation)

                val keyboardLocation = IntArray(2)
                this@CustomKeyboardView.getLocationOnScreen(keyboardLocation)

                val fieldY = fieldLocation[1]
                val keyboardY = keyboardLocation[1]

                if (fieldY > keyboardY) {
                    val deltaY = (fieldY - keyboardY)
                    val scrollTo = (fieldParent.scrollY + deltaY + this.measuredHeight + 10.toDp)
                    fieldParent.smoothScrollTo(0, scrollTo)
                }
                break
            }
            fieldParent = fieldParent.parent
        }
    }

    enum class KeyboardType {
        NUMBER,
        NUMBER_DECIMAL,
        QWERTY
    }
}
