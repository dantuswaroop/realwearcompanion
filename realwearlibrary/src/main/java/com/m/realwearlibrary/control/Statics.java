package com.m.realwearlibrary.control;

import android.app.Application;

/**
 * Common keys used throughout the app to access data from AppScope.
 */
public class Statics {
    public static Application APP;
}
