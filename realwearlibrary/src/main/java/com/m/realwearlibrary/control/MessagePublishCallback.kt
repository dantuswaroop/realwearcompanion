package com.m.realwearlibrary.control

abstract class MessagePublishCallback {
    abstract fun onSuccess()
    fun onFailure() { // most likely the caller does not need to know about failure
    }
}