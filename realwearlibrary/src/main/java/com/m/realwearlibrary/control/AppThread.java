package com.m.realwearlibrary.control;

import android.os.Handler;
import android.os.HandlerThread;
import android.os.Looper;

import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

import timber.log.Timber;

public final class AppThread {
    private final static int THREAD_DELAY = 0;

    private static ExecutorService bgnd;
    private static Handler nui;
    private static Handler ui;
    private static Handler db;

    private static boolean started = false;
    private static boolean testMode = false;

    public static void start() {
        if (started) return;
        started = true;

        ui = new Handler(Looper.getMainLooper());

        final HandlerThread nuiThread = new HandlerThread("non-ui");
        nuiThread.start();
        nui = new Handler(nuiThread.getLooper());

        final HandlerThread dbThread = new HandlerThread("db", android.os.Process.THREAD_PRIORITY_BACKGROUND);
        dbThread.start();
        db = new Handler(dbThread.getLooper());

        bgnd = Executors.newCachedThreadPool();
    }

    public static void stop() {
        if (null != nui) nui.getLooper().quit();
        if (null != db) db.getLooper().quit();
        if (null != bgnd) bgnd.shutdown();

        bgnd = null;
        nui = null;
        ui = null;
        db = null;

        started = false;
    }

    public static boolean uiThreadCheck() { return Looper.getMainLooper().getThread() == Thread.currentThread(); }

    public static boolean nuiThreadCheck() { return null != Looper.myLooper() && Looper.myLooper().equals(nui.getLooper()); }

    public static boolean dbThreadCheck() { return null != Looper.myLooper() && Looper.myLooper().equals(db.getLooper()); }

    public static void uiRemove(final Runnable runner) { ui.removeCallbacks(runner); }

    public static void nuiRemove(final Runnable runner) {
        nui.removeCallbacks(runner);
    }

    public static void nuiRemove(final Runnable runner, final String token) {
        nui.removeCallbacks(runner, token);
    }

    public static void bgndRemove(final Future<?> runner, final boolean force) {
        if (null != runner) runner.cancel(force);
    }

    /**
     * Note: do not use bgndPost for thread-unsafe operations
     */
    public final static Future<?> bgndPost(final String msg, final int priority, final Runnable run) {
        final Runnable runner = () -> {
                final long startTime = System.nanoTime();
                try {
                    run.run();
                } catch (Throwable ex) {
                    Timber.e(ex);
                } finally {

                    Timber.d("BGND THREAD " + msg + " (" + (System.nanoTime() - startTime) / 1000000 + "ms)");
                }
            };
        if (testMode) {
            runner.run();
            return null;
        } else {
            return bgnd.submit(runner);
        }

    }

    public static <V> V bgndGet(Callable<V> callable) {
        try {
            if (uiThreadCheck()) {
                Future<V> future = bgnd.submit(callable);
                return future.get();
            } else {
                return callable.call();
            }
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    public static Future<?> bgndPost(final String msg, final Runnable run) { return bgndPost(msg, 10, run); }

    public static Runnable nuiPost(final String msg, final Runnable run, final long delay) {
        final Runnable runner = () -> {
            final long startTime = System.nanoTime();
            try {
                run.run();
            } catch (Throwable ex) {
                Timber.e(ex);
            } finally {
                final String msg1 = "NON-UI THREAD " + (null == msg ? "" : msg) + " (" + (System.nanoTime() - startTime) / 1000000 + "ms)";
                Timber.d(msg1);
            }
        };
        if (testMode) {
            runner.run();
        } else {
            nui.postDelayed(runner, delay);
        }
        return runner;
    }

    public static Runnable dbPost(final String msg, final Runnable run, final long delay) {
        final Runnable runner = () -> {
            final long startTime = System.nanoTime();
            try {
                run.run();
            } catch (Throwable ex) {
                Timber.e(ex);
            } finally {
                final String msg1 = "DB THREAD " + (null == msg ? "" : msg) + " (" + (System.nanoTime() - startTime) / 1000000 + "ms)";
                Timber.d(msg1);
            }
        };

        if (testMode) {
            runner.run();
        } else {
            db.postDelayed(runner, delay);
        }

        return runner;
    }

    public static Runnable uiPost(final String msg, final Runnable run, final long delay) {
        final Runnable runner = () -> {
            final long startTime = System.nanoTime();
            try {
                run.run();
            } catch (Throwable ex) {
                Timber.e(ex);
            } finally {
                final String msg1 = "UI THREAD " + (null == msg ? "" : msg) + " (" + (System.nanoTime() - startTime) / 1000000 + "ms)";
                Timber.d(msg1);
            }
        };
        if (testMode) {
            runner.run();
        } else {
            ui.postDelayed(runner, delay);
        }
        return runner;
    }

    public static Runnable nuiPost(final String msg, final Runnable run) { return nuiPost(msg, run, THREAD_DELAY); }

    public static Runnable dbPost(final String msg, final Runnable run) { return dbPost(msg, run, THREAD_DELAY); }

    public static Runnable uiPost(final String msg, final Runnable run) { return uiPost(msg, run, THREAD_DELAY); }

    public static final class TestAccess {
        public static void start() {
            AppThread.testMode = true;
        }
        public static void reset() {
            AppThread.testMode = false;
        }
    }

}
