package com.m.realwearlibrary.control;

/**
 * A callback that needs to be run on completion of another task.
 *
 * @param <T> The result of the task that is passed to the action
 */
@FunctionalInterface
public interface CompletionCallback<T> {
    void execute(T result);
}
