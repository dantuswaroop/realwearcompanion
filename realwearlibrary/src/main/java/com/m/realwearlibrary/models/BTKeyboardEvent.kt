package com.m.realwearlibrary.models

data class BTKeyboardEvent(val event: KeyboardEvent,
                           val text: String,
                           val imeOptions: Int = -1,
                           val value: Int = -1)

enum class KeyboardEvent {
    InputType,
    Text,
    KeyCode
}

enum class RequestType {
    InputType
}


data class Request(val request: RequestType)